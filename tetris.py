import os
import pygame
import randomizer
from tetromino import Tetromino
from game_area import GameArea
from menu import Menu


text_color = (255, 255, 255)

# screen variables
min_screen_coord = (0, 0)
max_screen_coord = (960, 720)
screen_res = max_screen_coord

# game parameters
block_width, block_height = 32, 32
columns = 10
rows = 20
FPS = 60
display_fps = False
display_time = True

# coordinates
score_coord = (15, 50)
time_coord = (15, 100)
fps_coord = (15, 150)
menu_coord = (screen_res[0] * 3 / 5, screen_res[1] / 5)
next_tetromino_coord = (screen_res[0] / 10, (screen_res[1] / 10) * 6)
# next (text surface) coordinates
next_coord = (next_tetromino_coord[0], next_tetromino_coord[1] - 3 * block_height)

# game resources
font_file = 'prstartk.ttf'


def write(font, message, color):
    text = font.render(str(message), True, color)
    text = text.convert_alpha()

    return text


def main():
    pygame.init()
    pygame.display.set_caption("Tetris")
    screen = pygame.display.set_mode(screen_res)
    pygame.key.set_repeat(100, 100)

    blocks_path = os.path.join('sprites', 'blocks.png')
    game_background_path = os.path.join('sprites', 'background.png')
    menu_background_path = os.path.join('sprites', 'menu_background.png')
    grid_background_path = os.path.join('sprites', 'grid_background.png')

    # load background
    background = pygame.image.load(game_background_path).convert()
    background = pygame.transform.scale(background, screen.get_size())

    # load font
    font = pygame.font.Font(os.path.join('fonts', font_file), 18)

    # load and scale all blocks (each tetromino has its own block color)
    color_blocks = []
    blocks = pygame.image.load(blocks_path).convert()
    blocks = pygame.transform.scale(blocks, (block_width * 7, block_height))
    for i in range(7):
        block_surface = blocks.subsurface(i * block_width, 0, block_width, block_height)
        color_blocks.append(block_surface)

    next_surface = write(font, "NEXT", text_color)

    # create menu
    menu_items = ['Play', 'Quit']
    main_menu = Menu('Tetris', menu_background_path, screen_res, font_file, menu_coord)

    for item in menu_items:
        main_menu.add_item(item)

    # when game_over is True return to main menu
    while True:
        randomizer.reset()
        display_menu = True
        while display_menu:
            main_menu.show(screen)
            pygame.display.flip()

            # wait for keyboard input
            event = pygame.event.wait()
            user_input = main_menu.check_input(event)

            if user_input == menu_items[0]:
                display_menu = False
            elif user_input == menu_items[1] or event.type == pygame.QUIT:  # quit
                exit()

        # create game area
        game_area = GameArea(columns, rows, (block_width, block_height), screen_res,
                             grid_background_path)
        # current random tetromino
        current_tetromino = Tetromino((block_width, block_height), color_blocks,
                                      game_area.get_center_coord(), game_area.get_min_coord(),
                                      game_area.get_max_coord())
        # create next random tetromino
        next_tetromino = Tetromino((block_width, block_height), color_blocks,
                                   next_tetromino_coord, min_screen_coord, max_screen_coord)

        game_over = False
        game_paused = False
        clock = pygame.time.Clock()
        total_time = 0.0

        dt = 1.0 / FPS
        accumulator = 0.0

        while not game_over:
            # check keyboard input
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    game_over = True
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                    current_tetromino.move_right()
                    # if tetromino collides undo move right
                    if game_area.collides(current_tetromino.get_coords()):
                        current_tetromino.move_left()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                    current_tetromino.move_left()
                    # if tetromino collides undo move left
                    if game_area.collides(current_tetromino.get_coords()):
                        current_tetromino.move_right()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
                    current_tetromino.rotate_cw()
                    # if tetromino collides undo rotate clockwise
                    if game_area.collides(current_tetromino.get_coords()):
                        current_tetromino.rotate_ccw()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
                    current_tetromino.rotate_ccw()
                    # if tetromino collides undo rotate counterclockwise
                    if game_area.collides(current_tetromino.get_coords()):
                        current_tetromino.rotate_cw()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                    # increase tetromino speed
                    current_tetromino.speed_up()
                elif event.type == pygame.KEYUP and event.key == pygame.K_SPACE:
                    # reset tetromino speed (normal speed)
                    current_tetromino.reset_speed()
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_p:
                    # Pause the game
                    pause = True
                    game_area.display_message(screen, font, text_color, 'Pause')
                    while pause:
                        event = pygame.event.wait()
                        if event.type == pygame.KEYDOWN and event.key == pygame.K_p:
                            pause = False
                        elif event.type == pygame.QUIT:
                            exit()
                    game_paused = True

            if game_paused:
                frame_time = dt
                game_paused = False
                clock.tick(FPS)
            else:
                frame_time = clock.tick(FPS) / 1000.0  # convert to seconds
            total_time += frame_time

            if display_time:
                time_string = "Time " + '{0:02d}'.format(int(total_time // 60))\
                              + ":" + '{0:02d}'.format(int(total_time % 60))
                time_surface = write(font, time_string, text_color)
            if display_fps:
                fps_string = "Fps: " + str(int(clock.get_fps()))
                fps_surface = write(font, fps_string, text_color)

            score_string = "Score: " + str(game_area.get_score())
            score_surface = write(font, score_string, text_color)

            accumulator += frame_time
            while accumulator >= dt and not game_over:
                bottom = current_tetromino.move_down(dt)
                collision = game_area.collides(current_tetromino.get_coords())
                # tetromino collided with a block or reached bottom
                if collision or bottom:
                    game_area.update_grid(current_tetromino.get_coords(), current_tetromino.get_color_index())
                    current_tetromino = next_tetromino
                    current_tetromino.set_coords(game_area.get_center_coord(),
                                                 game_area.get_min_coord(), game_area.get_max_coord())
                    next_tetromino = Tetromino((block_width, block_height), color_blocks,
                                               next_tetromino_coord, min_screen_coord, max_screen_coord)
                accumulator -= dt
                game_over = game_area.is_game_over()

            # Draw
            screen.blit(background, min_screen_coord)
            screen.blit(score_surface, score_coord)
            if display_time:
                screen.blit(time_surface, time_coord)
            if display_fps:
                screen.blit(fps_surface, fps_coord)
            screen.blit(next_surface, next_coord)
            game_area.show(screen, color_blocks)
            current_tetromino.show(screen)
            next_tetromino.show(screen)
            pygame.display.flip()

        game_area.display_message(screen, font, text_color, 'Game Over')
        clock.tick(0.7)


if __name__ == '__main__':
    main()
