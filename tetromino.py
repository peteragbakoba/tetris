import pygame
import randomizer
from random import randint


class Tetromino:
    # I tetromino
    i_tetromino_angle_0 = [".#..",
                           ".#..",
                           ".#..",
                           ".#.."]

    # rotate 90 degrees cw (clockwise)
    i_tetromino_angle_90 = ["....",
                            "####",
                            "....",
                            "...."]

    i_tetromino = (i_tetromino_angle_0, i_tetromino_angle_90,
               i_tetromino_angle_0, i_tetromino_angle_90)

    # O tetromino
    o_tetromino_angle_0 = ["....",
                           ".##.",
                           ".##.",
                           "...."]

    o_tetromino = (o_tetromino_angle_0, o_tetromino_angle_0,
               o_tetromino_angle_0, o_tetromino_angle_0)

    # T tetromino
    t_tetromino_angle_0 = ["....",
                           "###.",
                           ".#..",
                           "...."]

    # rotate 90 degrees cw
    t_tetromino_angle_90 = [".#..",
                            "##..",
                            ".#..",
                            "...."]

    # rotate 180 degrees cw
    t_tetromino_angle_180 = [".#..",
                             "###.",
                             "....",
                             "...."]

    # rotate 270 degrees cw
    t_tetromino_angle_270 = [".#..",
                             ".##.",
                             ".#..",
                             "...."]

    t_tetromino = (t_tetromino_angle_0, t_tetromino_angle_90,
                   t_tetromino_angle_180, t_tetromino_angle_270)

    # J tetromino
    j_tetromino_angle_0 = ["....",
                           "###.",
                           "..#.",
                           "...."]

    # rotate 90 degrees cw
    j_tetromino_angle_90 = [".#..",
                            ".#..",
                            "##..",
                            "...."]

    # rotate 180 degrees cw
    j_tetromino_angle_180 = ["#...",
                             "###.",
                             "....",
                             "...."]

    # rotate 270 degrees cw
    j_tetromino_angle_270 = [".##.",
                             ".#..",
                             ".#..",
                             "...."]

    j_tetromino = (j_tetromino_angle_0, j_tetromino_angle_90,
                   j_tetromino_angle_180, j_tetromino_angle_270)

    # L tetromino cw
    l_tetromino_angle_0 = ["....",
                           "###.",
                           "#...",
                           "...."]

    # rotate 90 degrees cw
    l_tetromino_angle_90 = ["##..",
                            ".#..",
                            ".#..",
                            "...."]

    # rotate 180 degrees cw
    l_tetromino_angle_180 = ["..#.",
                             "###.",
                             "....",
                             "...."]

    # rotate 270 degrees cw
    l_tetromino_angle_270 = [".#..",
                             ".#..",
                             ".##.",
                             "...."]

    l_tetromino = (l_tetromino_angle_0, l_tetromino_angle_90,
                   l_tetromino_angle_180, l_tetromino_angle_270)

    # S tetromino
    s_tetromino_angle_0 = [".##.",
                           "##..",
                           "....",
                           "...."]

    # rotate 90 degrees cw
    s_tetromino_angle_90 = [".#..",
                            ".##.",
                            "..#.",
                            "...."]

    s_tetromino = (s_tetromino_angle_0, s_tetromino_angle_90,
                   s_tetromino_angle_0, s_tetromino_angle_90)

    # Z tetromino
    z_tetromino_angle_0 = ["##..",
                           ".##.",
                           "....",
                           "...."]

    # rotate 90 degrees cw
    z_tetromino_angle_90 = ["..#.",
                            ".##.",
                            ".#..",
                            "...."]

    z_tetromino = (z_tetromino_angle_0, z_tetromino_angle_90,
                   z_tetromino_angle_0, z_tetromino_angle_90)

    tetrominoes = (i_tetromino, o_tetromino, t_tetromino, j_tetromino,
                   l_tetromino, s_tetromino, z_tetromino)

    normal_speed = 120
    fast_speed = 1000

    def __init__(self, block_dimensions, color_blocks, coord, min_coord, max_coord):
        """
        Create a random tetromino.
        """
        self.random_index = randomizer.get_number()
        self.random_tetromino = Tetromino.tetrominoes[self.random_index]
        self.block_color = color_blocks[self.random_index]

        # select a random frame
        self.current_angle = randint(0, 3)
        self.current_frame = self.random_tetromino[self.current_angle]

        self.block_width = block_dimensions[0]
        self.block_height = block_dimensions[1]

        self.speed = Tetromino.normal_speed

        # game bounds
        self.min_coord = tuple(min_coord)
        self.max_coord = (max_coord[0] - self.block_width,
                          max_coord[1] - self.block_height)
        # coordinates of the center block (required for the rotation)
        self.center_coord = list(coord)

        # coordinates for each block of the tetromino
        # list of coordinates [x, y]
        self.blocks_coords = self.build()

    def build(self):
        """
        Calculates the coordinates of each block that is part of the tetromino.
        center block index is (1, 1)
        :return: list of coordinates [x, y] or empty list if a block is out of bounds
        """
        x, y = [self.center_coord[0] - self.block_width, self.center_coord[1]
                - self.block_height]
        tetromino_coord = []

        for i in range(len(self.current_frame)):
            for char in self.current_frame[i]:
                if char == '#':
                    if self.min_coord[0] <= x <= self.max_coord[0]:
                        tetromino_coord.append([x, y])
                    else:
                        return []
                x += self.block_width
            x = self.center_coord[0] - self.block_width
            y += self.block_height
        return tetromino_coord

    def set_coords(self, center_coord, min_coord, max_coord):
        self.min_coord = tuple(min_coord)
        self.max_coord = (max_coord[0] - self.block_width,
                          max_coord[1] - self.block_height)
        self.center_coord = list(center_coord)
        self.blocks_coords = self.build()

    def get_coords(self):
        return self.blocks_coords

    def get_color_index(self):
        return self.random_index

    def speed_up(self):
        self.speed = Tetromino.fast_speed

    def reset_speed(self):
        self.speed = Tetromino.normal_speed

    def show(self, screen):
        """
        Blit all blocks.
        :param screen: screen surface
        """
        for coord in self.blocks_coords:
            screen.blit(self.block_color, coord)

    def move_down(self, time):
        """
        Move down all blocks.
        :return: True if tetromino reached bottom
        """
        # check if all blocks are inside the bounds
        for coord in self.blocks_coords:
            if coord[1] >= self.max_coord[1]:  # reached the bottom of the game area
                return True

        # move all blocks
        distance = round(self.speed * time)
        for coord in self.blocks_coords:
            coord[1] += distance
        self.center_coord[1] += distance
        return False

    def move_left(self):
        """
        Move left all blocks if it is allowed.
        """
        accept_move = True
        # check if all blocks are inside the bounds
        for coord in self.blocks_coords:
            if coord[0] <= self.min_coord[0]:
                accept_move = False
                break

        if accept_move:
            self.center_coord[0] -= self.block_width
            for coord in self.blocks_coords:
                coord[0] -= self.block_width

    def move_right(self):
        """
        Move right all blocks if it is allowed.
        """
        accept_move = True
        # check if all blocks are inside the bounds
        for coord in self.blocks_coords:
            if coord[0] >= self.max_coord[0]:
                accept_move = False
                break

        if accept_move:
            self.center_coord[0] += self.block_width
            for coord in self.blocks_coords:
                coord[0] += self.block_width

    def rotate_ccw(self):
        """
        Rotate the tetromino 90 degrees counterclockwise.
        """
        temp_angle = self.current_angle
        if self.current_angle == 0:
            temp_angle = 3
        else:
            temp_angle -= 1

        self.current_frame = self.random_tetromino[temp_angle]
        temp_coords = self.build()

        if not temp_coords:  # rotation is not allowed
            # reset the current frame
            self.current_frame = self.random_tetromino[self.current_angle]
        else:
            self.current_angle = temp_angle
            self.blocks_coords = temp_coords

    def rotate_cw(self):
        """
        Rotate the tetromino 90 degrees clockwise.
        """
        temp_angle = (self.current_angle + 1) % 4
        self.current_frame = self.random_tetromino[temp_angle]
        temp_coords = self.build()
        if not temp_coords:  # rotation is not allowed
            # reset the current frame
            self.current_frame = self.random_tetromino[self.current_angle]
        else:
            self.current_angle = temp_angle
            self.blocks_coords = temp_coords
