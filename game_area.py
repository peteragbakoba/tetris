import pygame


class GameArea:

    def __init__(self, columns, rows, block_dimensions, screen_res, background_path):
        self.score = 0
        self.game_over = False

        self.block_width = block_dimensions[0]
        self.block_height = block_dimensions[1]

        self.area_width = columns * self.block_width
        self.area_height = rows * self.block_height

        # top left corner of game area
        self.min_coord = (screen_res[0] / 2.1,
                          (screen_res[1] - self.area_height) / 2)
        # bottom right corner of game area
        self.max_coord = (self.min_coord[0] + self.area_width,
                          self.min_coord[1] + self.area_height)

        # center coord of game area
        self.center_coord = (self.min_coord[0] + (columns // 2) * self.block_width,
                             self.min_coord[1])

        self.columns = columns
        self.rows = rows
        # rows x columns grid
        self.grid = [[-1 for i in range(self.columns)] for j in range(self.rows)]

        self.background = pygame.image.load(background_path).convert()
        self.background = pygame.transform.scale(self.background, (self.area_width, self.area_height))

    def collides(self, tetromino_coords):
        """
        Detects collision between blocks.
        :param tetromino_coords: list of coordinates (x, y)
        :return: True if blocks collide
        """

        for coord in tetromino_coords:
            column_index = int((coord[0] - self.min_coord[0]) // self.block_width)
            row_index = int((coord[1] + self.block_height - self.min_coord[1]) // self.block_height)

            if row_index >= self.rows or self.grid[row_index][column_index] >= 0:
                return True

        return False

    def is_game_over(self):
        return self.game_over

    def get_grid_indexes(self, coords):
        """
        Convert coordinates to grid indexes.
        :param coords: list of coordinates (x, y)
        :return: list of indexes (row, column)
        """
        grid_index_list = []
        for coord in coords:
            column_index = int((coord[0] - self.min_coord[0]) // self.block_width)
            row_index = int((coord[1] - self.min_coord[1]) // self.block_height)
            grid_index_list.append((row_index, column_index))
        return grid_index_list

    def update_grid(self, coords, color_index):
        """
        Converts coordinates to grid indexes using get_grid_indexes
        and assigns color_index to the corresponding cells.
        :param coords: list of coordinates (x, y)
        """

        indexes_list = self.get_grid_indexes(coords)
        for row_index, column_index in indexes_list:
            if row_index >= 0 and column_index >= 0:
                self.grid[row_index][column_index] = color_index

        # search for full rows
        for row_index, column_index in indexes_list:
            # check game over
            if row_index == 0:  # there is a block at the first row
                self.game_over = True

            full_row = True
            for j in range(self.columns):
                if self.grid[row_index][j] == -1:  # cell is empty
                    full_row = False
                    break

            # delete the row if it is full
            if full_row:
                del self.grid[row_index]
                self.score += 1
                # insert a new line at the beginning of the grid
                self.grid.insert(0, [-1 for i in range(self.columns)])

    def show(self, screen, color_blocks):
        """
        Blit background and all blocks.
        :param screen: screen surface
        :param color_blocks: block sprites tuple
        """
        screen.blit(self.background, self.min_coord)
        for i in range(self.rows):
            for j in range(self.columns):
                if self.grid[i][j] >= 0:  # if cell isn't empty (empty -> -1)
                    # cell value is color index
                    color_index = self.grid[i][j]
                    coord_x = self.min_coord[0] + j * self.block_width
                    coord_y = self.min_coord[1] + i * self.block_height
                    screen.blit(color_blocks[color_index], (coord_x, coord_y))

    def display_message(self, screen, font, color, message):
        text_surface = font.render(str(message), True, color).convert_alpha()
        text_x = self.min_coord[0] + (self.area_width - text_surface.get_width()) / 2
        text_y = self.min_coord[1] + self.area_height / 2
        screen.blit(text_surface, (text_x, text_y))
        pygame.display.flip()

    def get_score(self):
        return self.score

    def get_min_coord(self):
        return self.min_coord

    def get_max_coord(self):
        return self.max_coord

    def get_center_coord(self):
        return self.center_coord
